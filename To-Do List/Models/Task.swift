//
//  Task.swift
//  To-Do List
//
//  Created by Obi-Voin Kenobi on 1/20/19.
//  Copyright © 2019 Obi-Voin Kenobi. All rights reserved.
//

import RealmSwift

class Task: Object {
    
    @objc dynamic var id: String = NSUUID().uuidString
    @objc dynamic var title: String = ""
    @objc dynamic var hasCompleted: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
