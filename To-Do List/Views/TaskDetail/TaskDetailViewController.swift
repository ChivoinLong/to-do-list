//
//  TaskDetailViewController.swift
//  To-Do List
//
//  Created by Obi-Voin Kenobi on 1/20/19.
//  Copyright © 2019 Obi-Voin Kenobi. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MaterialComponents

protocol TaskDetailViewControllerDelegate: class {
    
    /// Invoked when user dismissed the view controller or successfully updated/saved task
    func taskViewControllerDidFinish()
}

class TaskDetailViewController: UIViewController, Storyboarded {

    // MARK: - IBOutlets
    @IBOutlet weak var taskTextField: MDCTextField!
    @IBOutlet weak var saveTaskButton: MDCRaisedButton!
    
    // MARK: - Local Variables
    public weak var delegate: TaskDetailViewControllerDelegate?
    public var viewModel: TaskViewModelType!
    private var taskController: MDCTextInputControllerUnderline?
    private let disposeBag = DisposeBag()
}


// MARK: - View Controller Life Cycles
extension TaskDetailViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set empty title for back bar button
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.setupMaterialViews()
        self.bindEvents()
        self.bindData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.taskTextField.becomeFirstResponder()
    }
}

// MARK: - Binding
extension TaskDetailViewController {
    fileprivate func bindData() {
        self.viewModel.task
            .asObservable()
            .subscribe(onNext: { [weak self] (task) in
                guard let self = self else { return }
                guard let task = task else { return }
                
                self.taskTextField.text = task.title
                self.saveTaskButton.setTitle("UPDATE", for: .normal)
            })
            .disposed(by: disposeBag)
    }
    
    fileprivate func bindEvents() {
        self.taskTextField.rx
            .text.map { !($0?.isEmpty ?? true) }
            .bind(to: self.saveTaskButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        self.saveTaskButton.rx.tap
            .throttle(1, latest: true, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                
                self.view.endEditing(true)
                
                let task = Task()
                if let id = try! self.viewModel.task.value()?.id {
                    task.id = id
                }
                
                task.title = self.taskTextField.text ?? ""
                self.viewModel.saveTask(task: task)
                
                self.delegate?.taskViewControllerDidFinish()
            })
            .disposed(by: disposeBag)
    }
}


// MARK: - Custom Views
extension TaskDetailViewController {
    fileprivate func setupMaterialViews() {
        self.taskController = MDCTextInputControllerUnderline(textInput: self.taskTextField)
        self.taskController?.textInputFont = UIFont.systemFont(ofSize: 16)
        self.taskController?.floatingPlaceholderActiveColor = UIColor.primaryColor
        self.taskController?.activeColor = UIColor.primaryColor
        self.taskController?.placeholderText = "Task"
        self.taskTextField.clearButtonMode = .whileEditing
        
        self.saveTaskButton.layer.cornerRadius = 25
    }
}


// MARK: - Events
extension TaskDetailViewController {
    @objc public func deleteTask() {
        self.view.endEditing(true)
        
        let alertController = MDCAlertController(title: "Remove Task", message: "Do you want to remove this task from your To-Do List?")
        alertController.titleColor = .primaryColor
        alertController.messageColor = .primaryTextColor
        alertController.buttonTitleColor = .primaryColor
        
        alertController.addAction(MDCAlertAction(title: "REMOVE", handler: { [weak self] (_) in
            guard let self = self else { return }
            
            self.viewModel.deleteTask()
            self.delegate?.taskViewControllerDidFinish()
        }))
        alertController.addAction(MDCAlertAction(title: "CANCEL", handler: nil))
        
        self.present(alertController, animated:true)
    }
}
