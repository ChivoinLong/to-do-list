//
//  TaskTableViewCell.swift
//  To-Do List
//
//  Created by Obi-Voin Kenobi on 1/20/19.
//  Copyright © 2019 Obi-Voin Kenobi. All rights reserved.
//

import UIKit
import MaterialComponents

class TaskTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var containerView: MDCCard!
    @IBOutlet weak var toDoTitleLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    
    // MARK: - Property & Actions
    var tapAction: () -> Void = {}
    var completeAction: () -> Void = {}
    var task: Task? {
        didSet {
            guard let task = task else { return }
            
            self.toDoTitleLabel.text = task.title

            if task.hasCompleted {
                // dim the cell, change check image, strikethrough title
                self.containerView.alpha = 0.8
                self.checkButton.setImage(#imageLiteral(resourceName: "ic_check"), for: .normal)
                
                let attributeString = NSMutableAttributedString(string: task.title)
                let range = NSMakeRange(0, attributeString.length)
                attributeString.addAttribute(.strikethroughStyle, value: 1, range: range)
                attributeString.addAttribute(.foregroundColor, value: UIColor.primaryTextColor.withAlphaComponent(0.6), range: range)
                
                self.toDoTitleLabel.attributedText = attributeString
            }
            else {
                self.containerView.alpha = 1
                self.checkButton.setImage(#imageLiteral(resourceName: "ic_uncheck"), for: .normal)
                self.toDoTitleLabel.textColor = .primaryTextColor
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.containerView.setShadowElevation(ShadowElevation.cardPickedUp, for: UIControl.State.normal)
        self.containerView.setShadowColor(UIColor.black.withAlphaComponent(0.5), for: .normal)
        self.containerView.layer.cornerRadius = containerView.frame.height / 2
        
        self.containerView.addTarget(self, action: #selector(tap), for: .touchUpInside)
        self.checkButton.addTarget(self, action: #selector(completeTask), for: .touchUpInside)
    }
}


// MARK: - Actions
extension TaskTableViewCell {
    @objc fileprivate func tap() {
        self.tapAction()
    }
    
    @objc fileprivate func completeTask() {
        self.completeAction()
    }
}
