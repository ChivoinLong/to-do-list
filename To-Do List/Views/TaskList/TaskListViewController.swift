//
//  TaskListViewController.swift
//  To-Do List
//
//  Created by Obi-Voin Kenobi on 1/19/19.
//  Copyright © 2019 Obi-Voin Kenobi. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Lottie
import MaterialComponents

protocol TaskListViewControllerDelegate: class {
    
    /// Invoked when user requests to create a new task
    func userDidRequestToCreateNewTask()
    
    /// Invoked when user requests showing Task item detail
    ///
    /// - Parameter item: Task item to show detail
    func userDidRequestItemDetail(item: Task)
}


class TaskListViewController: UIViewController, Storyboarded {

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addTaskFloatingButton: MDCFloatingButton!
    @IBOutlet weak var addTaskButtonContainerView: UIView!
    @IBOutlet weak var emptyTaskView: LOTAnimationView!
    
    // MARK: - Local Variables
    public weak var delegate: TaskListViewControllerDelegate?
    public var viewModel: HomeViewModelType!
    private let disposeBag = DisposeBag()
}


// MARK: - View Controller Life Cycles
extension TaskListViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bindTableView()
        self.bindEvents()
        self.viewModel.readTasks()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Showing feature highlight condition
        if !UserDefaults.standard.bool(forKey: "self.chivoin.To-Do-List.already_launch_before") {
            self.highlightFeature()
            UserDefaults.standard.set(true, forKey: "self.chivoin.To-Do-List.already_launch_before")
        }
    }
}


// MARK: - TableView Binding & Configurations
extension TaskListViewController: UITableViewDelegate {
    fileprivate func bindTableView() {
        // bind tableView
        self.viewModel.tasksList
            .asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: TaskTableViewCell.Identifier, cellType: TaskTableViewCell.self)) { (row, element, cell) in
                cell.task = element
                
                if !element.hasCompleted {
                    cell.tapAction = { [weak self] in
                        guard let self = self else { return }
                        guard !element.hasCompleted else { return }
                        self.delegate?.userDidRequestItemDetail(item: element)
                    }
                    
                    cell.completeAction = { [weak self] in
                        guard let self = self else { return }
                        self.viewModel.completeTask(task: element)
                    }
                }
            }
            .disposed(by: disposeBag)
        
        // show/hide emptyView
        self.viewModel.tasksList
            .map({ !($0.isEmpty) })
            .subscribe(onNext: { (hasData) in
                self.emptyTaskView.isHidden = hasData
                
                if !hasData {
                    self.emptyTaskView.setAnimation(named: "empty_box.json")
                    self.emptyTaskView.loopAnimation = true
                    self.emptyTaskView.animationSpeed = 0.5
                    self.emptyTaskView.play()
                }
                else {
                    self.emptyTaskView.stop()
                }
            })
            .disposed(by: disposeBag)
    }
}


// MARK: - Bind Events
extension TaskListViewController {
    fileprivate func bindEvents() {
        // add task button tap
        self.addTaskFloatingButton.rx.tap
            .throttle(1, latest: true, scheduler: MainScheduler.instance)
            .bind { [weak self] in
                guard let self = self else { return }
                self.delegate?.userDidRequestToCreateNewTask()
            }
            .disposed(by: disposeBag)
        
        // tableView item selected
        self.tableView.rx
            .modelSelected(Task.self)
            .asDriver()
            .drive(onNext: { [weak self] (task) in
                guard let self = self else { return }
                guard !task.hasCompleted else { return }
                self.delegate?.userDidRequestItemDetail(item: task)
            })
            .disposed(by: disposeBag)
    }
}


// MARK: - Feature Highlight
extension TaskListViewController {
    fileprivate func highlightFeature() {
        let highlightController = MDCFeatureHighlightViewController(highlightedView: self.addTaskButtonContainerView) { [weak self] (accepted) in
            guard let self = self else { return }
            
            if accepted {
                self.delegate?.userDidRequestToCreateNewTask()
            }
        }
        
        highlightController.titleText = "It's good to see you!"
        highlightController.bodyText = "Now, let's start by creating a new task."
        highlightController.titleColor = .white
        highlightController.bodyColor = .white
        highlightController.outerHighlightColor = UIColor.primaryColor.withAlphaComponent(kMDCFeatureHighlightOuterHighlightAlpha)
        present(highlightController, animated: true, completion:nil)
    }
}
