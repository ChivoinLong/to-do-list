//
//  RealmService.swift
//  To-Do List
//
//  Created by Obi-Voin Kenobi on 1/20/19.
//  Copyright © 2019 Obi-Voin Kenobi. All rights reserved.
//

import RealmSwift

class RealmService {
    
    // MARK: - Dependency Injection
    var realm: Realm
    
    init(realm: Realm) {
        self.realm = realm
        print(realm.configuration.fileURL?.absoluteString ?? "")
    }
    
    
    /// Read objects from realm
    ///
    /// - Parameter type: Object type
    /// - Returns: Result of list objects
    func read<T: Object>(_ type: T.Type) -> Results<T> {
        return self.realm.objects(type)
    }
    
    /// Write new object, or update the existing one
    ///
    /// - Parameter object: Object to write
    func write(object: Object) {
        try! realm.write {
            realm.add(object, update: true)
        }
    }
    
    /// Delete object from realm
    ///
    /// - Parameter object: Object to delete
    func delete(object: Object) {
        try! realm.write {
            realm.delete(object)
        }
    }
}
