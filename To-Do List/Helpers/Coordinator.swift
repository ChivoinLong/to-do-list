//
//  Corrdinator.swift
//  To-Do List
//
//  Created by Obi-Voin Kenobi on 1/20/19.
//  Copyright © 2019 Obi-Voin Kenobi. All rights reserved.
//

import UIKit
import Swinject

protocol Coordinator {
    var navigationController: UINavigationController { get }
    var container: Container { get }

    func start()
}
