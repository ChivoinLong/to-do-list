//
//  Date+Ext.swift
//  To-Do List
//
//  Created by Obi-Voin Kenobi on 1/20/19.
//  Copyright © 2019 Obi-Voin Kenobi. All rights reserved.
//

import Foundation

extension Date {

    static var now: Date {
        let now = Date()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let str = formatter.string(from: now)
        
        formatter.timeZone = TimeZone(secondsFromGMT: 0)!
        return formatter.date(from: str)!
    }
    
    init?(string: String, formart: String = "yyyy-MM-dd HH:mm") {
        let formartter = DateFormatter()
        formartter.dateFormat = formart
        formartter.timeZone = TimeZone(secondsFromGMT: 0)
        
        if let date = formartter.date(from: string) {
            self = date
        }
        else {
            return nil
        }
    }
    
    func toString(formart: String = "yyyy-MM-dd HH:mm", timezone: Bool = true) -> String {
        let formartter = DateFormatter()
        formartter.dateFormat = formart
        if timezone {
            formartter.timeZone = TimeZone(secondsFromGMT: 0)
        }
        
        let string = formartter.string(from: self)
        
        return string
    }
}
