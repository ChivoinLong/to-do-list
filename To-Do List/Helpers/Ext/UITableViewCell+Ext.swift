//
//  UITableViewCell+Ext.swift
//  To-Do List
//
//  Created by Obi-Voin Kenobi on 1/20/19.
//  Copyright © 2019 Obi-Voin Kenobi. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    static var Identifier: String {
        return String(describing: self)
    }
}
