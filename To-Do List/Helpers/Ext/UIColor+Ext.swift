//
//  UIColor+Ext.swift
//  To-Do List
//
//  Created by Obi-Voin Kenobi on 1/20/19.
//  Copyright © 2019 Obi-Voin Kenobi. All rights reserved.
//

import UIKit

extension UIColor {
    
    public static var primaryColor: UIColor {
        return UIColor(red: 227/255, green: 33/255, blue: 26/255, alpha: 1)
    }
    
    public static var primaryDarkColor: UIColor {
        return UIColor(red: 186/255, green: 28/255, blue: 22/255, alpha: 1)
    }
    
    public static var secondaryColor: UIColor {
        return UIColor(red: 0/255, green: 200/255, blue: 83/255, alpha: 1)
    }
    
    public static var backgroundColor: UIColor {
        return UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1)
    }
    
    public static var primaryTextColor: UIColor {
        return UIColor(red: 80/255, green: 80/255, blue: 80/255, alpha: 1)
    }
    
    public static var secondaryTextColor: UIColor {
        return primaryColor.withAlphaComponent(0.8)
    }
    
//    public static var errorColor: UIColor {
//        return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
//    }
}
