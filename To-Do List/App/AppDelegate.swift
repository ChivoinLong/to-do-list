//
//  AppDelegate.swift
//  To-Do List
//
//  Created by Obi-Voin Kenobi on 1/19/19.
//  Copyright © 2019 Obi-Voin Kenobi. All rights reserved.
//

import UIKit
import Swinject
import RealmSwift
import RevealingSplashView
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    internal let container = Container()
    var coordinator: MainCoordinator?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.toolbarDoneBarButtonItemImage = #imageLiteral(resourceName: "ic_keyboard_down")
        IQKeyboardManager.shared.toolbarTintColor = .primaryColor
        
        setupDependencies()
        
        // create the main navigation controller to be used for our app
        let navController = UINavigationController()
        navController.navigationBar.tintColor = UIColor.primaryColor
        navController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navController.navigationBar.shadowImage = UIImage()
        navController.navigationBar.isTranslucent = false
        navController.view.backgroundColor = .white
        
        // send that into our coordinator so that it can display view controllers
        coordinator = MainCoordinator(container: container, navigationController: navController)
        
        // tell the coordinator to take over control
        coordinator?.start()
        
        // create a basic UIWindow and activate it
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        
        //Initialize a revealing Splash with with the iconImage, the initial size and the background color
        let revealingSplashView = RevealingSplashView(iconImage: #imageLiteral(resourceName: "Mad Logo - red"), iconInitialSize: CGSize(width: 309, height: 100), backgroundColor: UIColor.white)
        revealingSplashView.animationType = SplashAnimationType.popAndZoomOut
        
        //Adds the revealing splash view as a sub view
        window?.addSubview(revealingSplashView)
        
        //Starts animation
        revealingSplashView.startAnimation()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}


// MARK: - Set up the depedency graph in the DI container
extension AppDelegate {
    internal func setupDependencies() {
        // service
        container.register(RealmService.self) { (_) -> RealmService in
            return RealmService(realm: try! Realm())
        }.inObjectScope(.container)
        
        // viewModels
        container.register(HomeViewModelType.self) { (resolver) -> HomeViewModelType in
            return HomeViewModel(service: resolver.resolve(RealmService.self)!)
        }
        container.register(TaskViewModelType.self) { (resolver) -> TaskViewModelType in
            return TaskViewModel(service: resolver.resolve(RealmService.self)!)
        }
        
        // viewControllers
        container.register(TaskListViewController.self) { (resolver) -> TaskListViewController in
            let vc = TaskListViewController.instantiate()
            vc.viewModel = resolver.resolve(HomeViewModelType.self)
            
            return vc
        }
        container.register(TaskDetailViewController.self) { (resolver) -> TaskDetailViewController in
            let vc = TaskDetailViewController.instantiate()
            vc.viewModel = resolver.resolve(TaskViewModelType.self)
            
            return vc
        }
    }
}
