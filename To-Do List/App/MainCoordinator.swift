//
//  MainCoordinator.swift
//  To-Do List
//
//  Created by Obi-Voin Kenobi on 1/20/19.
//  Copyright © 2019 Obi-Voin Kenobi. All rights reserved.
//

import UIKit
import Swinject

class MainCoordinator: Coordinator {
    var container: Container
    var navigationController: UINavigationController
    
    init(container: Container, navigationController: UINavigationController) {
        self.container = container
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = self.container.resolve(TaskListViewController.self)!
        vc.delegate = self
        vc.title = "To-Do List"
        navigationController.pushViewController(vc, animated: false)
    }
    
    private func newTask() {
        let vc = self.container.resolve(TaskDetailViewController.self)!
        vc.delegate = self
        vc.title = "New Task"
        
        navigationController.pushViewController(vc, animated: true)
    }
    
    private func showDetail(task: Task) {
        let vc = self.container.resolve(TaskDetailViewController.self)!
        vc.delegate = self
        vc.viewModel.task.onNext(task)
        vc.title = "Update Task"
        
        let deleteButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_delete"), style: .plain, target: vc, action: #selector(vc.deleteTask))
        vc.navigationItem.rightBarButtonItem = deleteButton
        
        navigationController.pushViewController(vc, animated: true)
    }
}


extension MainCoordinator: TaskListViewControllerDelegate {
    func userDidRequestToCreateNewTask() {
        self.newTask()
    }
    
    func userDidRequestItemDetail(item: Task) {
        self.showDetail(task: item)
    }
}

extension MainCoordinator: TaskDetailViewControllerDelegate {
    func taskViewControllerDidFinish() {
        if navigationController.viewControllers.count > 1,
            let homeViewController = navigationController.viewControllers[navigationController.viewControllers.count - 2] as? TaskListViewController {
            homeViewController.viewModel.readTasks()
        }

        navigationController.popViewController(animated: true)
    }
}
