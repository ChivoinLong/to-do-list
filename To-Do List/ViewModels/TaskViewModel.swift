//
//  TaskViewModel.swift
//  To-Do List
//
//  Created by Obi-Voin Kenobi on 1/20/19.
//  Copyright © 2019 Obi-Voin Kenobi. All rights reserved.
//

import RxSwift
import RealmSwift

protocol TaskViewModelType: class {
    /// Realm Service for accessing realm objects
    var service: RealmService { get set }
    
    /// Task for showing detail & update
    var task: BehaviorSubject<Task?> { get set }
    
    /// Save a new task object or update the existing one based on Parameter
    ///
    /// - Parameter task: Task object to save or update
    func saveTask(task: Task)
    
    /// Remove a task object from realm
    func deleteTask()
}


class TaskViewModel: TaskViewModelType {
    
    var service: RealmService
    var task: BehaviorSubject<Task?>
    
    init(service: RealmService) {
        self.service = service
        self.task = BehaviorSubject<Task?>(value: nil)
    }
    
    
    func saveTask(task: Task) {
        self.service.write(object: task)
    }
    
    func deleteTask() {
        guard let task = try! self.task.value() else { return }
        self.service.delete(object: task)
    }
}
