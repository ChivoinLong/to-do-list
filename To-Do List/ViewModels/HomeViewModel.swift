//
//  HomeViewModel.swift
//  To-Do List
//
//  Created by Obi-Voin Kenobi on 1/20/19.
//  Copyright © 2019 Obi-Voin Kenobi. All rights reserved.
//

import RxSwift
import RealmSwift

protocol HomeViewModelType: class {
    /// Realm Service for accessing realm objects
    var service: RealmService { get }
    
    /// Task objects for tableView binding
    var tasksList: BehaviorSubject<[Task]> { get }
    
    /// Read all task objects from realm
    func readTasks()
    
    /// Update task object to completed state
    ///
    /// - Parameter task: Task object to update
    func completeTask(task: Task)
}


class HomeViewModel: HomeViewModelType {
    
    var service: RealmService
    var tasksList: BehaviorSubject<[Task]>
    
    init(service: RealmService) {
        self.service = service
        self.tasksList = BehaviorSubject<[Task]>(value: [])
    }
    

    func readTasks() {
        let tasks = self.service.read(Task.self).sorted(byKeyPath: "hasCompleted")
        tasksList.onNext(tasks.compactMap({ $0 }))
    }
    
    func completeTask(task: Task) {
        let updatedTask = Task()
        updatedTask.id = task.id
        updatedTask.title = task.title
        updatedTask.hasCompleted = true
        
        self.service.write(object: updatedTask)
        
        self.readTasks()
    }
}
